package com.xiaoshi.test;

import com.xiaoshi.test.entity.TestEntity;
import com.xiaoshi.test.mapper.TestMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.UUID;

@SpringBootTest
class TestApplicationTests {

    @Resource
    TestMapper testMapper;

    @Test
    void contextLoads() {
    }

    @Test
    void test() {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        System.out.println(uuid);
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDate localDate = localDateTime.toLocalDate();
        LocalTime localTime = localDateTime.toLocalTime();

        TestEntity testEntity1 = new TestEntity(
                uuid, "求可涵", 18, true, 100d, Date.valueOf(localDate),
                Timestamp.valueOf(localDateTime), Time.valueOf(localTime), "长得高");
        int i = testMapper.insertTest(testEntity1);
        if (i > 0) {
            System.out.println("插入成功！");
        }

        List<TestEntity> testList = testMapper.getTestList(); // alt+enter
        for (TestEntity testEntity : testList) { // testList.for
            System.out.println(testEntity.toString()); // sout
        }
    }
}
