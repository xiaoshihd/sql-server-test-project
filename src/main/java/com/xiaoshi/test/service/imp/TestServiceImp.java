package com.xiaoshi.test.service.imp;

import com.xiaoshi.test.entity.TestEntity;
import com.xiaoshi.test.mapper.TestMapper;
import com.xiaoshi.test.service.TestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TestServiceImp implements TestService {
    @Resource
    TestMapper testMapper;

    @Override
    public List<TestEntity> getTestList() {
        return testMapper.getTestList();
    }

    @Override
    public int insertTest(TestEntity testEntity) {
        return testMapper.insertTest(testEntity);
    }

    @Override
    public TestEntity getTestByGuid(String guid) {
        // 写逻辑
        TestEntity testByGuid = testMapper.getTestByGuid(guid);
        testByGuid.setDesc("大笨蛋");
        return testByGuid;
    }
}
