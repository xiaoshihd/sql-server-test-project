package com.xiaoshi.test.service;

import com.xiaoshi.test.entity.TestEntity;

import java.util.List;

public interface TestService {
    public List<TestEntity> getTestList();

    int insertTest(TestEntity testEntity);

    TestEntity getTestByGuid(String guid);
}
