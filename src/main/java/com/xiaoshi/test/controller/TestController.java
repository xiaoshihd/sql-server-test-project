package com.xiaoshi.test.controller;

import com.xiaoshi.test.entity.TestEntity;
import com.xiaoshi.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    TestService testService;

    @GetMapping(value = "/getTestList")
    public List<TestEntity> getTestList() {
        return testService.getTestList();
    }

    @PostMapping(value = "/insertTest")
    int insertTest(@RequestBody TestEntity testEntity) {
        // 参数校验
        return testService.insertTest(testEntity);
        // 统一接口返回值
    }

    @GetMapping(value = "getTestByGuid")
    TestEntity getTestByGuid(@RequestParam(value = "guid") String guid) {
        return testService.getTestByGuid(guid);
    }

}
