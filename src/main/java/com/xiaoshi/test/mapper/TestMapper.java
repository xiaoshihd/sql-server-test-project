package com.xiaoshi.test.mapper;

import com.xiaoshi.test.entity.TestEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description: Test表Mapper层
 * @Author: 师鹏飞
 * @Date: 2021/6/11
 * @Time： 9:30
 */
@Mapper
public interface TestMapper {
    String test_table = "test";

    @Select(
            "SELECT * FROM " + test_table
    )
    List<TestEntity> getTestList();

    @Select(
            "SELECT * FROM " + test_table + " WHERE guid = #{guid}"
    )
    TestEntity getTestByGuid(@Param("guid") String guid);

    // #{} 加 ‘’, ${}
    @Insert(
            "INSERT INTO test ( guid, name, age, sex, money, sign_date, sign_datetime, sign_time, [desc] ) " +
                    "VALUES " +
                    "(#{guid},#{name},#{age},#{sex},#{money},#{sign_date},#{sign_datetime},#{sign_time},#{desc})"
    )
    int insertTest(TestEntity testEntity);
}
